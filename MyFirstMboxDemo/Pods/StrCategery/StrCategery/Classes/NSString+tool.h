//
//  NSString+tool.h
//  TestForTXL
//
//  Created by 唐叶甫 on 2020/1/20.
//  Copyright © 2020 tyf. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (tool)
- (NSString *)getUnNull;
@end

NS_ASSUME_NONNULL_END
