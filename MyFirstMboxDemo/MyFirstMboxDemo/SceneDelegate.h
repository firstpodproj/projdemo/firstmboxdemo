//
//  SceneDelegate.h
//  MyFirstMboxDemo
//
//  Created by a201111 on 2022/1/14.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

